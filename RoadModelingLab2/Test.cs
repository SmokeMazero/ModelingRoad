﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoadModelingLab2
{
    class Test
    {
        private Experiment _experiment; //эксперимент с заданными параметрами


        public Test(
              double speedAverage,
              double lengthSection,
              double timeBetweenCars,
              double timeRed,
              double timeGreen,
              double mathExpectationOneSide,
              double mathExpectationTwoSide,
              int countIterationOfExperiment
            )
        {
            _experiment = new Experiment(
                new ParamTask(
                    lengthSection: lengthSection,
                    timeRed: timeRed,
                    timeGreen: timeGreen,
                    timeBetwenCars: timeBetweenCars,
                    mathExpectationOneSide: mathExpectationOneSide,
                    mathExpectationTwoSide: mathExpectationTwoSide,
                    speedAverage: speedAverage,
                    countIteration: countIterationOfExperiment
                    )
             );

            TraceOperations.addHead("Движение автомобилей на аварийном участке дороге");

            _experiment.runExperiment();
        }

        public override string ToString()
        {
            return $"Результаты экспермента: {_experiment}";
        }
    }
}
