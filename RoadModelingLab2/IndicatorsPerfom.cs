﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoadModelingLab2
{
    class IndicatorsPerfom
    {
        private int[] _countCarSide; //количество автомобилей, проехавших через перекрёсток

        private double[] _timeInQueue; //Среднее время ожидания автомобилей, находящихся в очереди

        private double[] _timeTravel; //Среднее время проезда автомобилем ремонтируемого участка дороги
 
        public IndicatorsPerfom()
        {
            this._countCarSide = new int[2];
            this._timeTravel = new double[2];
            this._timeInQueue = new double[2];
        }

        public void setValue(int indexSide, double timeInQueue, double timeTravel)
        {
            this._countCarSide[indexSide]++;
            this._timeInQueue[indexSide] += timeInQueue;
            this._timeTravel[indexSide] += timeTravel;
        }

        private double getAverageTimeInQueue(int indexSide) => (_timeInQueue[indexSide] / _countCarSide[indexSide]);

        private double getAverageTimeTravel(int indexSide) => (_timeTravel[indexSide] / _countCarSide[indexSide]);

        public void setTimeTravel(int index, double time)
        {
            this._timeTravel[index] += time;
        }

        public int getCount(int indexSide) => _countCarSide[indexSide];

        public override string ToString()
        {
            string temp ="";
            for(int i = 0; i < 2; i++)
            {
                temp += $"\n\n" +
                $"Номер очереди :{i+1}\n"+
                $"Количество машин, проехавших через участок дороги: {_countCarSide[i]}\n" +
                $"Среднее время ожидания машин: {getAverageTimeInQueue(i)}(c)\n" +
                $"Среднее время проезда автомобилей через аварийный участок дороги: {getAverageTimeTravel(i)}(c)";
            }
            return temp;
            
        }
    }
}
