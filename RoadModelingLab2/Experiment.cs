﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RoadModelingLab2
{
    class Experiment
    {
        private Queue[] _queues; //очереди для двух сторон выбранного участка дороги
        private ParamTask _paramTask; //параметры задачи
        private IndicatorsPerfom _indicators; //показатели эффективности
        private double _globalTimeExperiment; //Время эксперимента
        private static statesExperiment _statesExperiment; //Состояние эксперимента
        private static stateTraficLight _statesTraficLightSides; //Состояние светофоров с каждой стороны
        private int _iteration; //номер итерации эксперимента
        private double timeSide; //время на текущем светофоре
        private double timeInProces; //время проезда машины
        private int numberCarInqueue = -1; //номер заявки в очереди

        public Experiment(ParamTask paramTask)
        {
            this._paramTask = paramTask;

            this._indicators = new IndicatorsPerfom();

            this._globalTimeExperiment = 0;

            _statesExperiment = statesExperiment.run;

            _statesTraficLightSides = stateTraficLight.green;

            this._queues = new Queue[2];
            this._queues[0] = new Queue(new ExponentialGenerator().getRand, (int)paramTask.MathExpectationOneSide);
            this._queues[1] = new Queue(new ExponentialGenerator().getRand, (int)paramTask.MathExpectationTwoSide);
        }

        public void runExperiment()
        {
            double timeFinalise1 = 0.0;
            double timeFinalise2 = 0.0;

            while (_iteration < _paramTask.Iter)
            {
                _globalTimeExperiment += (_statesTraficLightSides == stateTraficLight.green ? _paramTask.TimeGreen : _paramTask.TimeRed);

                TraceOperations.addGlobalTime(_globalTimeExperiment);

                _queues[0].ProcessInQueue((_statesTraficLightSides == stateTraficLight.green ? _paramTask.TimeGreen : _paramTask.TimeRed), _paramTask, _indicators, out _indicators, _statesTraficLightSides == stateTraficLight.green ? stateTraficLight.green : stateTraficLight.red, 0, out timeFinalise1, timeFinalise2);

                timeFinalise1 = 0.0;

                _queues[1].ProcessInQueue((_statesTraficLightSides == stateTraficLight.green ? _paramTask.TimeRed : _paramTask.TimeGreen), _paramTask, _indicators, out _indicators, _statesTraficLightSides == stateTraficLight.green ? stateTraficLight.red : stateTraficLight.green, 1, out timeFinalise2, timeFinalise1);

                timeFinalise2 = 0.0;

                if (_statesTraficLightSides == stateTraficLight.red)
                {
                    _iteration++;
                }

                _statesTraficLightSides = (_statesTraficLightSides == stateTraficLight.green ? stateTraficLight.red : stateTraficLight.green);
            }
            _statesExperiment = statesExperiment.stop;
        }

        public override string ToString()
        {
            return $"Эксперимент по моделированию движения машин на аварийном участке дороги с регулировкой светофорами\n" +
                $"Общее время эксперимента: {_globalTimeExperiment}(c)\n" +
                $"Параметры системы: {_paramTask}\n" +
                $"Просчитанные показатели эффективности модели: {_indicators}\n";
        }
    }
}
