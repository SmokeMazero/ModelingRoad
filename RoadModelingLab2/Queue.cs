﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoadModelingLab2
{
    class Queue
    {
        private List<Apply> applies; //все заявки в очереди

        private int _full; //количество машин, находящихся в очереди
        public int Full
        {
            get;
        }

        private Func<double, double> _funcAllocation; //функция распределения, по закону которой будет произведена генерация очереди.
        private int _mathExpectation;

        public Queue(
            Func<double, double> funcAllocation,
            int mathExpectation
            )
        {
            this._funcAllocation = funcAllocation;
            this._full = 0;
            this._mathExpectation = mathExpectation;

            this.applies = new List<Apply>();
        }

        private double generate(int indexApply)
        {
            double timeModeling = _funcAllocation(_mathExpectation);
            this._full++;

            this.applies.Add(new Apply(timeModeling, indexApply));
            
            return timeModeling;
        }

        public void ProcessInQueue(
            double timeSide, 
            ParamTask param, 
            IndicatorsPerfom indicatorsIn ,out IndicatorsPerfom indicators, 
            stateTraficLight stateTraficLightIn,
            int indexSide,
            out double timeFinaliseOut,
            double timeFinaliseIn
            )
        {
            int numberCarInQueue = this._full;
            indicators = indicatorsIn;

            timeFinaliseOut = 0.0;

            while (true)
            {
                numberCarInQueue++;

                double tempSide = generate(numberCarInQueue);

                timeSide -= tempSide;

                timeSide -= timeFinaliseIn;

                TraceOperations.addTimeInSide(indexSide, timeSide);

                if (timeSide >= 0)
                {    
                    if (stateTraficLightIn == stateTraficLight.green)
                    {
                        if (this._full == 1)
                        {
                            indicators.setValue(indexSide, Math.Abs(tempSide - timeFinaliseIn), param.LengthSection / ParamTask.TranslateKminM(param.SpeedAverage));

                            TraceOperations.addTimeInQueue(indexSide, Math.Abs(tempSide - timeFinaliseIn));

                            if(timeSide - param.LengthSection / ParamTask.TranslateKminM(param.SpeedAverage) < 0)
                            {
                                timeFinaliseOut = Math.Abs(timeSide - param.LengthSection / ParamTask.TranslateKminM(param.SpeedAverage));

                                TraceOperations.addTimeFinalize(indexSide, timeFinaliseOut);
                            }

                            deleteApply();

                            TraceOperations.addDelCarInQueue(this._full);

                            numberCarInQueue--;
                        }
                        else
                        {
                            while(tempSide - param.TimeBetwenCars >=0)
                            {
                                if (this._full >= 1)
                                {
                                    timeSide -= param.TimeBetwenCars;

                                    TraceOperations.addTimeInSide(indexSide, timeSide);

                                    indicators.setTimeTravel(indexSide, param.LengthSection / ParamTask.TranslateKminM(param.SpeedAverage));

                                    TraceOperations.addSpeedCalc(indicators.getCount(indexSide), ParamTask.TranslateKminM(param.SpeedAverage));

                                    if (timeSide - timeSide - param.LengthSection / ParamTask.TranslateKminM(param.SpeedAverage) < 0)
                                    {
                                        timeFinaliseOut = Math.Abs(timeSide - param.LengthSection / ParamTask.TranslateKminM(param.SpeedAverage));

                                        TraceOperations.addTimeFinalize(indexSide, timeFinaliseOut);
                                    }

                                    deleteApply();

                                    TraceOperations.addDelCarInQueue(this._full);

                                    numberCarInQueue--;
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        indicators.setValue(indexSide, Math.Abs(timeSide) + param.TimeRed + param.TimeBetwenCars * numberCarInQueue, 0);

                        TraceOperations.addTimeInQueue(indexSide, Math.Abs(timeSide), param.TimeRed, param.TimeBetwenCars, numberCarInQueue);
                    }
                }
                else
                {
                    indicators.setValue(indexSide, Math.Abs(timeSide) + param.TimeRed, 0);

                    TraceOperations.addTimeInQueue(indexSide, Math.Abs(timeSide), param.TimeRed);
                    break;
                }
            }

            return;
        }

        public void deleteApply()
        {
            this._full--;
            this.applies.RemoveAt(this.applies.Count-1);
        }
    }
}
