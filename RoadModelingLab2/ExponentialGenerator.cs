﻿using System;

namespace RoadModelingLab2
{
    class ExponentialGenerator
    {
        private double _mathExpectation;
        public double MathExpectation
        {
            get { return _mathExpectation; }
            set { _mathExpectation = value; }
        }

        private static Random random = new Random();

        public double getRand(double mathExpectation)
        {
            _mathExpectation = mathExpectation;
            double gen = random.NextDouble();
            double ran = reverseFunc(gen);
            TraceOperations.addGeneratingCars(mathExpectation, gen, ran);
            return ran;
        }
        
        private double reverseFunc(double x) => - _mathExpectation * Math.Log(x);
    }
}
