﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoadModelingLab2
{
    static class TraceOperations
    {
        private static string _mainInformations;
        public static string MainInformations { get; set; }

        public static void addHead(string head) => _mainInformations += $"\nРасчёт для эксперимента {head}";

        public static void addSpeedCalc(params double[] list) => _mainInformations += $"\nV[{list[0]}] = {list[1]}/{list[2]}";

        public static void addTimeInQueue(params double[] list) => _mainInformations += $"\nT_(in Queue)[на стороне {list[0]}] = {list[1]} + {list[2]} + {list[3]}*{list[4]}";

        public static void addTimeArrive(params double[] list) => _mainInformations += $"\nT_(transmit car)[{list[0]}] = {list[1]} + {list[2]}";

        public static void addAverageTimeInQueue(params double[] list) => _mainInformations += $"\nT_(avarage in Queue)[{list[0]}] = Sum(Time in Queue)/{list[1]}";

        public static void addInkrementiteration(params double[] list) => _mainInformations += $"\niteration = {list[0]} + 1";

        public static void addTrace(string operations) => _mainInformations += "\n{operations}\n\r";

        public static void addIteration(string interation) => _mainInformations += $"\nИтерация {interation}: ";

        public static void addGlobalTime(params double[] list) => _mainInformations += $"\nВремя эксперимента на текущей итерации: TimeExperiment = {list[0]}";

        public static void addTimeFinalize(params double[] list) => _mainInformations += $"\nВремя, необходимое для завершения движения автомобилями на стороне {list[0]}: TimeFinalise = {list[1]}";

        public static void addGeneratingCars(params double[] list) => _mainInformations += $"\nПоявление новой заявки: t_(addTime) = -{list[0]} * ln({list[1]}) = {list[2]}";

        public static void addTimeInSide(params double[] list) => _mainInformations += $"\nТекущее оставшееся время горения света на стороне {list[0]}: T_(side) = {list[1]}";

        public static void addDelCarInQueue(params double[] list) => _mainInformations += $"\nОставшееся количество машин в очереди после удаления: {list[0]}";
    }
}
