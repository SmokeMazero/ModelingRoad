﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoadModelingLab2
{
    class ParamTask
    {
        private double _lengthSection; //длина дороги
        public double LengthSection { get { return _lengthSection; }}

        private double _timeRed; //время горения красного света светофора
        public double TimeRed { get { return _timeRed; } }

        private double _speedAverage; //средняя скорость проезда машиной перекрёстка
        public double SpeedAverage { get { return _speedAverage; } }

        private double _timeGreen; //время горения зелёного света светофора
        public double TimeGreen { get { return _timeGreen; } }

        private double _timeBetwenCars; //временной интервал между машинами в момент проезда аварийного участка дороги
        public double TimeBetwenCars { get { return _timeBetwenCars; } }

        private double _mathExpectationOneSide; //математическое ожидание 1 стороны
        public double MathExpectationOneSide { get { return _mathExpectationOneSide; } }

        private double _mathExpectationTwoSide; //математическое ожидание 2 стороны
        public double MathExpectationTwoSide { get { return _mathExpectationTwoSide; } }

        private int _countIterations;
        public int Iter { get { return _countIterations; } }

        public ParamTask(
            double lengthSection, 
            double timeRed, 
            double timeGreen, 
            double timeBetwenCars, 
            double mathExpectationOneSide, 
            double mathExpectationTwoSide, 
            double speedAverage,
            int countIteration
            )
        {
            this._lengthSection = lengthSection;
            this._timeRed = timeRed;
            this._timeGreen = timeGreen;
            this._timeBetwenCars = timeBetwenCars;
            this._mathExpectationOneSide = mathExpectationOneSide;
            this._mathExpectationTwoSide = mathExpectationTwoSide;
            this._speedAverage = speedAverage;
            this._countIterations = countIteration;
        }

        public static double TranslateKminM(double speedofKminHour) => ((speedofKminHour * 1000) / 3600.0);

        public override string ToString()
        {
            return $"\n\n" +
                $"Количество итераций, потребованных пользователем, для модеирования заданного эксперимента: {_countIterations}\n"+
                $"Длина аварийного участка дороги: {_lengthSection}(м)\n" +
                $"Время горения красного сигнала светофора: {_timeRed}(c)\n" +
                $"Время горения зелёного сигнала светофора: {_timeGreen}(c)\n" +
                $"Время, требуемое на разгрузку одной позиции очереди: {_timeBetwenCars}(c)\n" +
                $"Параметры распределения, по которым генерируются автомобили: \n" +
                $"Математическое ожидание для распределения на очереди левой стороны дороги: {_mathExpectationOneSide}\n" +
                $"Математическое ожидание для распределения на очереди правой стороны дороги: {_mathExpectationTwoSide}\n";
        }
    }
}
