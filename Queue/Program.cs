﻿using Queue;
using System;

namespace RoadModel
{
    class Program
    {
        static Test side1 = new Test(),
                    side2 = new Test();

        static void Main(string[] args) => outPutInformation();

        public static void outPutInformation()
        {
            string answer = "да";

            while (true)
            {
                side1.process(12, 0.1, 60, 500, 10, 0.01, 25);

                side2.process(15, 0.2, 60, 500, 10, 0.01, 25);

                Console.WriteLine($"Эксперимент для 1 стороны: \n{side1.ToString()}Эксперимент для 2 стороны: \n{side2.ToString()}");

                Console.WriteLine("Повторить эксперимент?(да\\нет) ");

                answer = Console.ReadLine() ?? answer;

                if (answer.Contains("д"))
                {
                    side1.rebuildExperiiment();

                    side2.rebuildExperiiment();

                    Console.Clear();
                }
                else
                {
                    break;
                }
            }
        }
    }
}
