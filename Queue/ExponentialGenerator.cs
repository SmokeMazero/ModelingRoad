﻿using System;

namespace RoadModel
{
    class ExponentialGenerator
    {
        private double lambda;

        private Random random = new Random();

        public ExponentialGenerator(double mathExpectation)
        {
            lambda = 1/mathExpectation;
        }

        public double getRand() => reverseFunc(random.NextDouble());

        private double reverseFunc(double x) => -1 / lambda * Math.Log(x);
    }
}
