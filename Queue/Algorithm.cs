﻿using Queue;
using System;
using System.Linq;


namespace RoadModel
{
    #region states
    enum statesTraficLight
    {
        Green, Red
    }
    enum statesSystem
    {
        experiment, endExperiment
    }
    #endregion

    class Algorithm
    {
        #region params

        private Queue[] queue; //Очереди для красного и зелёного
        private statesTraficLight stateTraficLight; //Текущее состояние светофора
        private statesSystem stateCurrentThread; //Текущее состояние системы
        private ParamsTask taskParameters; //Текущие параметры системы
        private OptimisationParams timeOptimisation; //Время зелёного света и среднее время зелёного света
        private int iteration;

        #endregion

        public Algorithm(int mathExpectation, double realibility, double speedAverage, double lengthSection, int drivewayCount, double timeAverage, int timeRed)
        {
            taskParameters = new ParamsTask(speedAverage, lengthSection, drivewayCount, timeAverage, timeRed);
            timeOptimisation = new OptimisationParams(taskParameters.DrivewayCount, taskParameters.DrivewayCount);
            queue = new Queue[] { new Queue(mathExpectation, realibility), new Queue(mathExpectation, realibility) };

            stateTraficLight = statesTraficLight.Green;

            generateQueue(statesTraficLight.Red);
            queue[0] = queue[0] + queue[1];
        }
        
        internal void ExperementationSide()
        {
            iteration = 0;

            while (true)
            {
                if (stateTraficLight == statesTraficLight.Green)
                {
                    timeOptimisation.TimeTLG[iteration] =
                        (int)(queue[(int)stateTraficLight].FullQueue * (taskParameters.LengthSection / taskParameters.SpeedAverage) +
                                              factor(queue[(int)stateTraficLight].FullQueue, taskParameters.TimeAverage));
                    queue[0].clear();

                    generateQueue(statesTraficLight.Green);

                    stateTraficLight = statesTraficLight.Red;

                    if (iteration < taskParameters.DrivewayCount - 1)
                    {
                        iteration++;
                    }
                    else
                    {
                        stateCurrentThread = statesSystem.endExperiment;
                    }
                }
                else
                {
                    queue[1].clear();

                    generateQueue(statesTraficLight.Red);

                    queue[0] = queue[0] + queue[1];

                    queue[1].clear();

                    timeOptimisation.AverageTimeQueue[iteration-1] = (taskParameters.TimeRed * queue[0].FullQueue + factor(queue[0].FullQueue, taskParameters.TimeAverage));

                    stateTraficLight = statesTraficLight.Green;
                }

                if (stateCurrentThread == statesSystem.endExperiment)
                {
                    break;
                }
            }
        }

        #region CaclAverage
        public double averageTGL() => timeOptimisation.AverageTimeGreenLigth();
        public double AverageTFiQ() => timeOptimisation.AverageTimeQueue.Sum()/timeOptimisation.AverageTimeQueue.Count();
        #endregion

        public void generateQueue(statesTraficLight statesTraficLight)
        {
            int tempTime = (statesTraficLight == statesTraficLight.Red ? taskParameters.TimeRed : timeOptimisation.TimeTLG[iteration]);
            int i = 0;
            while(tempTime >0)
            {
                queue[(int)stateTraficLight].generateQueue();
                tempTime -= (int)queue[(int)stateTraficLight].TimeGenerate;
                queue[(int)stateTraficLight].FullQueue++;
            }
            queue[(int)statesTraficLight].IsGenerating = true;
            taskParameters.CountGeneratingOnSide += $"[{queue[(int)statesTraficLight].FullQueue}];";

        }

        public double factor(int size, double coeff)
        {
            double result = 1;
            while (size != 0)
            {
                result += (size) * coeff;
                size--;
            }
            return result;
        }

        public override string ToString()
        {
            string tempTimeTLG = "";
            foreach(int i in timeOptimisation.TimeTLG)
            {
                tempTimeTLG += $"[{i}];";
            }

            return $"Параметры для заданной стороны: \n" +
                //$"Математическое ожидание: {queue[0].MathExcpectation}\n" +
                $"Время красного света на светофоре: {taskParameters.TimeRed} (с)\n" +
                $"Среднее время проезда 1 позиции очереди машиной: {taskParameters.TimeAverage} (с)\n" +
                $"Средняя скорость движения машин в текущем эксперименте: {taskParameters.SpeedAverage}(км/ч) - {Math.Round(ParamsTask.TranslateKminM(taskParameters.SpeedAverage), 2)}(м/c)\n" +
                /*$"Уровень достоверности генерации по экспоненциальному закону: {queue[0].GetReliability()}\n" +*/
                $"Количество сгенерированных автомобилей: {taskParameters.CountGeneratingOnSide}\n" +
                $"t_сф_зел_i = {tempTimeTLG}\n";
        }
    }
}
