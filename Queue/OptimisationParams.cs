﻿using System.Linq;

namespace Queue
{
    class OptimisationParams
    {
        private int[] timeTraficLightGreen; // время зелёного света, требуемое для проезда "заполненной" очереди за промежуток времени, когда автомобили стояли на красном свете светофора
        public int[] TimeTLG
        {
            get => timeTraficLightGreen;
            set { timeTraficLightGreen = value; }
        }

        private double[] averageTimeQueue; //среднее время нахождения в очереди
        public double[] AverageTimeQueue
        {
            get => averageTimeQueue;
            set { averageTimeQueue = value; }
        }

        public OptimisationParams(int N, int countExperiments)
        {
            timeTraficLightGreen = new int[N];
            averageTimeQueue = new double[countExperiments];
        }

        public double AverageTimeGreenLigth() => (double)timeTraficLightGreen.Sum() / (double)timeTraficLightGreen.Length;
    }
}
