﻿using RoadModel;

namespace Queue
{
    class Test
    {
        Algorithm testing; //Algorithm TwoSide = new Algorithm(6, 0.2, 20, 500, 10, 2, 55);

        public void process(int mathExpectation,
            double relibility,
            double SpeedAverage,
            double SectionArea,
            int CountExperiment,
            double TimeAverageInQueue,
            int TimeRedLight)
        {
            testing = new Algorithm(mathExpectation: mathExpectation, realibility: relibility, speedAverage: SpeedAverage, lengthSection:SectionArea, drivewayCount: CountExperiment, timeAverage: TimeAverageInQueue, timeRed: TimeRedLight);

            testing.ExperementationSide();
        }

        public void rebuildExperiiment() => testing.ExperementationSide();

        public override string ToString()
        {
            return testing.ToString() + "\n" +
                $"Полученные результаты:\n" +
                $"Среднее время горения зелёного света: {testing.averageTGL()} (c)\n" +
                $"Среднее время ожидания в очереди: {testing.AverageTFiQ()} (c)\n\n";
        }
    }
}
