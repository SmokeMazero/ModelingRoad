﻿namespace RoadModel
{
    class Queue
    {
        private int fullQueue; // количество машин в очереди
        public int FullQueue
        {
            set { fullQueue = value; }
            get { return fullQueue; }
        }

        private double timeGenerate;
        public double TimeGenerate
        {
            set { timeGenerate = value; }
            get { return timeGenerate; }
        }

        private bool isGenerating; // метка о генерации
        public bool IsGenerating
        {
            get { return isGenerating; }
            set { isGenerating = value; }
        }

        private int mathExcpectation;
        public int MathExcpectation
        {
            set { mathExcpectation = value; }
            get { return mathExcpectation; }
        }

        private double reliability; // уровень достоверности (уровень выбора по распределению)
        public double GetReliability() => reliability;

        private ExponentialGenerator generator;

        public Queue(int mathExcpectation, double reliability)
        {
            this.mathExcpectation = mathExcpectation;
            this.reliability = reliability;
            
            this.generator = new ExponentialGenerator(this.mathExcpectation);
        }

        public Queue(Queue queuetemp)
        {
            this.mathExcpectation = queuetemp.mathExcpectation;
            this.isGenerating = queuetemp.isGenerating;
            this.fullQueue = queuetemp.FullQueue;
            this.reliability = queuetemp.reliability;
            this.generator = queuetemp.generator;
        }
        public void generateQueue()
        {
            /*if (generator.getRand() <= reliability)
            {
                fullQueue += 1;
            }*/
            timeGenerate = generator.getRand();
        }
        public static Queue operator + (Queue left, Queue right)
        {
            left.fullQueue += right.fullQueue;
            return new Queue(left);
        } 

        public void clear() => fullQueue = 0;

        public void changeLabelGenerating() => isGenerating = isGenerating ? false : true ;

    }
}